# MLOps

This project is being conducted as part of the MLOps 3.0 course from the Open Data Science community.

## Getting started

A description of how to get started is contained in the file contributing.md

## Project management strategy

The project consists of 3 branches.
 - main - branch for the final version of the service or application being developed
 - dev - branch for developing a service or application(contains scripts with the .py extension)
 - exp - branch for conducting experiments(contains files with the extension .ipynb with any experiments performed and their results)

 ### Stages of an ML project

 1. EDA and experimentation phase is underway. The results are contained in the exp branch
 2. The ML application development stage is underway. The results are contained in the dev branch.
 3. The final stage is serving the ML application. The product version is contained in the main branch.

# Docker

1. To build an image with the mlops_app:latest tag, passing the current folder as the contex: 

    docker build -t mlops_app:latest ./

2. To launch a container called mlops_container use the command:

    docker run -it --name mlops_container --rm mlops_app:latest

3. To connect to the file system of a running container, use the command:

    docker exec -it mlops_container bash

## P.S. 

The repository will be updated as the course progresses.